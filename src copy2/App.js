import './App.css';
import AppNavbar from './components/AppNavbar.js'
import { Container } from 'react-bootstrap'
import Banner from './components/Banner.js'
import Highlights from './components/Highlights'

export default function App() {
  return (
    <>
      <AppNavbar />
      {/* Body */}
      <Container>
        <Banner />
        <Highlights />
      </Container>
    </>
  );
}
