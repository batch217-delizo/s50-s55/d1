import { Form, Button } from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react'
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

const Login = () => {

  const { user, setUser } = useContext(UserContext);
  // Use State
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('')
  const [isActive, setIsActive] = useState(true)


  // UseEffect -> manipualtion of dom  
  // UseEffect -> manipualtion of data using context
  useEffect(() => {
    console.log(email)
    if (email !== '' && password !== '') {

      setIsActive(true)
    } else {
      setIsActive(false)
    }
  })

  function authenticate(e) {
    e.preventDefault()

    // Once user successfully login in, the email will be stored in the localStorage of the browser
    localStorage.setItem("email", email)

    // Once browser local storage has 1 email stored setUser will send the data in to the context. Which can be used for data passing all
    setUser({
      email: localStorage.getItem("email")
    })

    setEmail('')
    setPassword('')
    console.log(` ${email} has been verified! Welcome back! `)
  }




  return (
    // Once user is authenticated, if successful, he/she will be directed
    (user.email) ?
      <Navigate to="/courses" />
      :

      <>
        <h1>Login</h1>
        <Form onSubmit={(e) => authenticate(e)}>
          {/* Email Section */}
          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" required
              onChange={e => setEmail(e.target.value)} value={email} />
          </Form.Group>

          {/* Password  1  */}
          <Form.Group className="mb-3" controlId="Password">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" required onChange={e => setPassword(e.target.value)} value={password} />
          </Form.Group>


          {
            isActive ?
              <Button variant="primary" type="submit" >
                Login
              </Button>

              :

              <Button variant="primary" type="submit" disabled>
                Login
              </Button>

          }

          {/* Submit Button */}

        </Form>
      </>
  )

}

export default Login