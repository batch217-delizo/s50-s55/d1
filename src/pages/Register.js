import { Form, Button } from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react'
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom'
const Register = () => {

  // Accepting the passed data from UserContext
  const { user } = useContext(UserContext)

  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')
  const [isActive, setIsActive] = useState(false)

  console.log(email)
  console.log(password1)
  console.log(password2)

  useEffect(() => {
    console.log(email)
    if ((email !== '' & password1 !== '' && password2 !== '') && (password1 == password2)) {

      setIsActive(true)
    } else {
      setIsActive(false)
    }
  })

  function registerUser(e) {
    e.preventDefault()

    setEmail('')
    setPassword1('')
    setPassword2('')

    alert("register successfully")
  }

  return (
    (user.email !== null) ?
      <Navigate to="/courses" />
      :
      <>
        <h1>Register</h1>
        <Form onSubmit={(e) => registerUser(e)} >
          {/* Email Section */}
          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" required
              onChange={e => setEmail(e.target.value)} value={email} />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          {/* Password  1 and 2 section */}
          <Form.Group className="mb-3" controlId="Password1">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" required onChange={e => setPassword1(e.target.value)} value={password1} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="Password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control type="password" placeholder="Verify Password" required onChange={e => setPassword2(e.target.value)} value={password2} />
          </Form.Group>

          {
            isActive ?
              <Button variant="primary" type="submit" >
                Submit
              </Button>

              :

              <Button variant="primary" type="submit" disabled>
                Submit
              </Button>

          }

          {/* Submit Button */}

        </Form>
      </>
  )
}

export default Register