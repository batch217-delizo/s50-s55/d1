import { Navigate } from 'react-router-dom'
import { useEffect, useContext } from 'react'
import UserContext from '../UserContext';

export default function Logout() {

  /*   localStorage.clear(); */

  const { unsetUser, setUser } = useContext(UserContext);

  // Clear the localStorage where the user information is stored.

  unsetUser();

  useEffect(() => {
    setUser({ email: null })
  })

  return (
    <Navigate to="/login" />
  )
}

