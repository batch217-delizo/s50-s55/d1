import { Card, Button } from 'react-bootstrap'
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react'

export default function CourseCard({ courseProp }) {
	//This will check if our data is passed successfully.
	//console.log(props);
	//console.log(typeof props);

	//Destructured Vaiable from courseProp
	const { name, description, price } = courseProp;

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);
	const [disabled, setDisabled] = useState(false)

	console.log(useState(0));

	function enroll() {
		if (seats > 0) {
			setCount(count + 1);
			console.log("Enrollees: " + count);

			setSeats(seats - 1);
			console.log("Seats: " + seats);
		} else {
			alert("No more seats available.");
		}
	}

	CourseCard.propTypes = {
		courseProp: PropTypes.shape({
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.string.isRequired
		})
	}

	useEffect(() => {
		if (seats === 0) {
			setDisabled(true)
			alert("mo more seats")
		}
	})

	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>

				<Card.Text>Seats: {seats}</Card.Text>
				<Card.Text>Enrollees: {count}</Card.Text>
				<Button disabled={disabled} variant="primary" onClick={enroll}>Enroll</Button>
			</Card.Body>
		</Card>
	)
}