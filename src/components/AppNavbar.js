import { Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom'

import { useContext } from 'react'
import UserContext from '../UserContext';

export default function AppNavbar() {

	// Store user information
	// const [user, setUser] = useState(localStorage.getItem("email"))
	// console.log(user)

	const { user } = useContext(UserContext)
	console.log(user.email + "my code")
	return (

		<Navbar className="p-3" bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">

					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/Courses">Courses</Nav.Link>
					{
						(user.email !== null) ?
							<div>
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>

							</div>
							:

							<>
								<Nav.Link as={NavLink} to="/Login">Login</Nav.Link>
								<Nav.Link as={NavLink} to="/Register">Registration</Nav.Link>
							</>
					}

				</Nav>
			</Navbar.Collapse>
		</Navbar>

	)
}