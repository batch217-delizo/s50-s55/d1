//Destructuring
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// import Button from 'react-bootstrap/Button';
// Bootstrap grid system components
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';


export default function Banner({ bannerProp }) {

	console.log(bannerProp)
	const { title, content, destination, label } = bannerProp


	return (
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>


	)
}