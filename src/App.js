import './App.css';
import AppNavbar from './components/AppNavbar.js'
import { Container } from 'react-bootstrap'
import Home from './pages/Home.js'
import Courses from './pages/Courses.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js';
import Error from './pages/Error.js';



import { Route, Routes } from 'react-router-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import { useContext, useState } from 'react';

import UserContext, { UserProvider } from './UserContext.js'

// useStates give data, values
// useEffect give effect to the behavior of element
export default function App() {
  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })

  // Function for localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }



  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        {/*HEAD*/}
        <AppNavbar />
        {/*BODY*/}
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/courses" element={<Courses />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>

      </Router>
    </UserProvider>

  );
}


