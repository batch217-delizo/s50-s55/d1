import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
//Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'




const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
    <App />
  </React.StrictMode>
);


/*
const name = "John Smith";

const user = {
  firstName: "Romenick",
  lastName: "Garcia"
}

function formatName(user){
  return user.firstName + " " + user.lastName;
}


const element = <h1>Hello, {formatName(user)}</h1>;

root.render(element);

*/